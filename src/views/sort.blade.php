@if($active)
	<?php $class = 'active';?>
@else
	<?php $class = '';?>
@endif

@if($direction == 'desc')
	<a class="sort {{$class}}" href="{{$url}}"><i class="fa fa-chevron-up"></i></a>
@else
	<a class="sort {{$class}}" href="{{$url}}"><i class="fa fa-chevron-down"></i></a>
@endif