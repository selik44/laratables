<?php namespace Gentlefox\Laratables\Filters;

class SelectFilter extends Filter {

	protected $defaultValue = '*';

	protected $values = [];

	/**
	 * @param  \Illuminate\Database\Query\Builder $query the database query.
	 * @return void
	 */
	public function apply(\Illuminate\Database\Eloquent\Builder $query)
	{
		if ($this->value == $this->defaultValue) return;

		$column = $this->column['machine'];
		if ($this->column['relation'] != null) {
			$column = $column .'_id';
		}

		$query->where($column, '=', $this->value);
	}

	public function setValues($values = [])
	{
		$this->values = $values;
	}

	public function buildUI() {
		$str = '<label for="filter_' .$this->column['machine'] .'">' .$this->label .'</label>';

		$str .= '<select name="filter_' .$this->column['machine'] .'">';

		foreach($this->values as $key => $value) {
			if ($key == $this->value) {
				$selected = ' selected';
			}else {
				$selected = '';
			}
			$str .= '<option' .$selected .' value="' .$key .'">' .$value .'</option>';
		}

		$str .= '</select>';

		return $str;
	}

}