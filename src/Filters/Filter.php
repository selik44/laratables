<?php namespace Gentlefox\Laratables\Filters;
use Illuminate\Http\Request;

/**
 * An abstract class describing a particular way to filter a column.
 */
abstract class Filter {

	private $request;

	/**
	 * The column this filter is operating on.
	 * @var array
	 */
	protected $column = [];

	protected $label = 'Label';

	protected $value = null;

	protected $defaultValue = null;

	protected $filterFunction = null;

	public function __construct(Request $request, $column) {
		$this->request = $request;
		$this->column = $column;
		$this->label = $column['human'];
		$this->value = $request->query('filter_' .$this->column['machine'], $this->defaultValue);
	}

	/**
	 * Set a custom filterFunction to use when applying the filter to the Query Builder, instead of the default.
	 * 
	 * It receives the BooleanFilter instance as the first argument and
	 * the Builder queryBuilder instance as the second.
	 * @param Closure|null $filterFunction the closure. Passing null will revert to using the default behavior.
	 */
	public function setFilterFunction($filterFunction = null) {
		$this->filterFunction = $filterFunction;
	}

	/**
	 * 
	 * @param string $label label
	 * @return  $this
	 */
	public function setLabel($label) {
		$this->label = $label;
		return $this;
	}

	/**
	 * Apply the filter to the query
	 * @param  \Illuminate\Database\Query\Builder $query the database query.
	 * @return void
	 */
	public abstract function apply(\Illuminate\Database\Eloquent\Builder $query);

	/**
	 * Use the filterFunction on the query
	 * @return void|mixed whatever the filterFunction returns.
	 */
	protected final function applyFilterFunction(\Illuminate\Database\Eloquent\Builder $query) {
		if ($this->filterFunction != null) {
			$closure = $this->filterFunction;
			return $closure($this, $query);
		}
	}

	public function getColumn() {
		return $this->column;
	}

	public function getValue() {
		return $this->value;
	}

	public abstract function buildUI();

}