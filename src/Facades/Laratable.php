<?php namespace Gentlefox\Laratables\Facades;

class Laratable extends \Illuminate\Support\Facades\Facade {

	public static function getFacadeAccessor() {
		return 'laratable';
	}

}