<?php namespace Gentlefox\Laratables;

use Gentlefox\Laratables\Filters\SearchFilter;
use Gentlefox\Laratables\Filters\SelectFilter;
use Gentlefox\Laratables\Filters\BooleanFilter;
use Illuminate\Database\Eloquent\Builder;

class Factory {

	private $request;

	public function __construct($request) {
		$this->request = $request;

		foreach([

			'escape' => function($model, $column, $value) {
				return htmlentities($value);
			},

			'limit' => function($model, $column, $value, $limit = 50, $end = '...') {
				return str_limit($value, $limit, $end);
			},

			'diffForHumans' => function($model, $column, $value) {
				return $value->diffForHumans();
			},

			'link' => function($model, $column, $value, $url) {
				return '<a href="' .$url .'">' .$value .'</a>';
			},

		] as $name => $transformer) {
			$this->registerTransformer($name, $transformer);
		}

		Laratable::registerFilter('search', SearchFilter::class);
		Laratable::registerFilter('select', SelectFilter::class);
		Laratable::registerFilter('boolean', BooleanFilter::class);

		foreach(config('laratables.filters', []) as $name => $class) {
			Laratable::registerFilter($name, $class);
		}
	}

	public function make(Builder $query, $columns = []) {
		return new Laratable($this->request, $query, $columns);
	}

	public function registerTransformer($name, $callable) {
		Laratable::registerTransformer($name, $callable);
	}

}